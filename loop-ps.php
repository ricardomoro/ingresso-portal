<?php while (have_posts()) : the_post(); ?>
    <div class="row">
        <div class="col-xs-12 center">
            <a href="<?php echo get_post_meta($post->ID, 'link', true); ?>">
            <?php if (has_post_thumbnail()): ?>
                <?php the_post_thumbnail(null, array('class' => 'img-responsive img-thumbnail', 'alt' => get_the_title())); ?>
            <?php else: ?>
                <?php the_title(); ?>
            <?php endif; ?>
            </a>
        </div>
    </div>
    <br/>
<?php endwhile; ?>
