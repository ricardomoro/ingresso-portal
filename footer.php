<a href="#fim-conteudo" id="fim-conteudo" class="sr-only">Fim do conte&uacute;do</a>

<footer class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <p>
                <!-- Wordpress -->
                <a href="http://www.wordpress.org/" target="blank">Desenvolvido com Wordpress<span class="sr-only"> (abre uma nova p&aacute;gina)</span></a> <span class="glyphicon glyphicon-new-window"></span>
                &mdash;
                <!-- Código-fonte GPL -->
                <a href="https://bitbucket.org/ricardomoro/ingresso-portal/" target="blank">C&oacute;digo-fonte deste tema sob a licen&ccedil;a GPLv3<span class="sr-only"> (abre uma nova p&aacute;gina)</span></a> <span class="glyphicon glyphicon-new-window"></span>
                &mdash;
                <!-- Creative Commons -->
                <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="blank"><img src="<?php echo get_template_directory_uri(); ?>/img/cc-by-nc-nd.png" alt="M&iacute;dia licenciada sob a Licen&ccedil;a Creative Commons Atribui&ccedil;&atilde;o-N&atilde;oComercial-SemDeriva&ccedil;&otilde;es 4.0 Internacional (abre uma nova p&aacute;gina)" /></a> <span class="glyphicon glyphicon-new-window"></span>
            </p>
        </div>
    </div>
</footer>

<!-- JavaScript -->
<?php wp_footer(); ?>
</body>
</html>
