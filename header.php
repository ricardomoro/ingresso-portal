<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index,follow">
    <meta name="author" content="Comunicação do IFRS">

    <!-- Título -->
    <title><?php
        /*
         * Print the <title> tag based on what is being viewed.
         */
        global $page, $paged;

        wp_title( '-', true, 'right' );

        // Add the blog name.
        bloginfo( 'name' );

        // Add the blog description for the home/front page.
        // $site_description = get_bloginfo( 'description', 'display' );
        // if ( $site_description && ( is_home() || is_front_page() ) )
        //     echo ' - '.$site_description;

        // Add a page number if necessary:
        if ( $paged >= 2 || $page >= 2 )
            echo ' - ' . sprintf( 'Página %s', max( $paged, $page ) );
    ?></title>

    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico"/>

    <!-- CSS & JS -->
    <?php wp_head(); ?>

    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css" />
</head>
<body>
<a href="#inicio-conteudo" accesskey="1" class="sr-only">Conteúdo <span class="badge">1</span></a>

<?php echo get_template_part('partials/barrabrasil'); ?>

<a href="#inicio-conteudo" id="inicio-conteudo" class="sr-only">In&iacute;cio do conte&uacute;do</a>
