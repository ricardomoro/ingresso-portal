<?php get_header(); ?>

<section class="container" id="conteudo">
<?php
    $args = array(
        'post_type' => 'ps',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field'    => 'slug',
                'terms'    => 'ativos',
            ),
        ),
    );
    query_posts($args);

    if (have_posts()):
        get_template_part('loop', 'ps');
    else:
?>
    <div class="row">
        <div class="col-xs-12">
            <h2>Processos Seletivos Anteriores</h2>
        </div>
    </div>

<?php
    $args = array(
        'post_type' => 'ps',
        'tax_query' => array(
            array(
                'taxonomy' => 'category',
                'field'    => 'slug',
                'terms'    => 'anteriores',
            ),
        ),
    );
    query_posts($args);

    if (have_posts()):
        get_template_part('loop', 'ps');
    else:
?>

    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-warning" role="alert">
                <p><strong>Aten&ccedil;&atilde;o!</strong> N&atilde;o existem Processos Seletivos cadastrados.</p>
            </div>
        </div>
    </div>
<?php endif; ?>
</section>

<?php get_footer(); ?>
