<?php

function load_styles_portal() {
    wp_enqueue_style('css-bootstrap');
}

function load_scripts_portal() {
    wp_enqueue_script('html5shiv');
    wp_enqueue_script('html5shiv-print');
    wp_enqueue_script('respond');
    wp_enqueue_script('respond-matchmedia');
    wp_enqueue_script('jquery');
    wp_enqueue_script('bootstrap');
    wp_enqueue_script('js-barra-brasil');
}

add_action( 'wp_enqueue_scripts', 'load_styles_portal', 2 );
add_action( 'wp_enqueue_scripts', 'load_scripts_portal', 2 );
