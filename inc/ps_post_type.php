<?php
// Register Custom Post Type
function ps_post_type() {
    $labels = array(
        'name'                => _x( 'Processos Seletivos', 'Post Type General Name', 'text_domain' ),
        'singular_name'       => _x( 'Processo Seletivo', 'Post Type Singular Name', 'text_domain' ),
        'menu_name'           => __( 'Processos Seletivos', 'text_domain' ),
        'parent_item_colon'   => __( 'Processo Seletivo pai:', 'text_domain' ),
        'all_items'           => __( 'Todos Processos Seletivos', 'text_domain' ),
        'view_item'           => __( 'Visualizar Processo Seletivo', 'text_domain' ),
        'add_new_item'        => __( 'Adicionar Novo Processo Seletivo', 'text_domain' ),
        'add_new'             => __( 'Adicionar Novo', 'text_domain' ),
        'edit_item'           => __( 'Editar Processo Seletivo', 'text_domain' ),
        'update_item'         => __( 'Atualizar Processo Seletivo', 'text_domain' ),
        'search_items'        => __( 'Procurar Processo Seletivo', 'text_domain' ),
        'not_found'           => __( 'Não encontrado', 'text_domain' ),
        'not_found_in_trash'  => __( 'Não encontrado na Lixeira', 'text_domain' ),
    );
    $args = array(
        'label'               => __( 'evento', 'text_domain' ),
        'description'         => __( 'Post para processos seletivos.', 'text_domain' ),
        'labels'              => $labels,
        'supports'            => array( 'title', 'thumbnail', ),
        'taxonomies'          => array( 'category', 'post_tag' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );
    register_post_type( 'ps', $args );

}

// Hook into the 'init' action
add_action( 'init', 'ps_post_type', 0 );


// Adicionar a meta box com a lista de sites.
add_action("admin_init", "admin_ps_init");
 
function admin_ps_init(){
    add_meta_box("link", "Site do Processo Seletivo", "link_meta", "ps", "normal", "high");
}

function link_meta() {
    global $post;
    if (isset($post)) {
        $custom = get_post_custom($post->ID);
        $link = (isset($custom["link"][0]) ? $custom["link"][0] : '');
    }
    ?>
        <p><label class="screen-reader-text" for="link_meta">Endere&ccedil;o do site do Processo Seletivo</label>
        <select id="link_meta" name="link">
    <?php
        $blog_list = wp_get_sites();
        foreach ($blog_list as $blog) :
            $full_path = $blog['domain'].$blog['path'];
    ?>
            <option <?php echo ($link == 'http://'.$full_path) ? 'selected="selected"' : ''; ?> value="http://<?php echo $full_path; ?>"><?php echo $full_path; ?></option>  
    <?php endforeach; ?>
        </select></p>
    <?php
}

// Salvar os campos personalizados.
add_action('save_post', 'save_link');

function save_link() {
    global $post;
    if (isset($post)) {
        update_post_meta($post->ID, "link", $_POST["link"]);
    }
}

// Mover a meta box da imagem destaque.
add_action('do_meta_boxes', 'ps_image_box');

function ps_image_box() {
    remove_meta_box('postimagediv', 'ps', 'side');
    add_meta_box('postimagediv', __('Featured Image'), 'post_thumbnail_meta_box', 'ps', 'normal', 'low');
}



// Editar as colunas da listagem.
add_action("manage_posts_custom_column",  "ps_custom_columns", 10, 2);
add_filter("manage_edit-evento_columns", "ps_edit_columns", 10, 1);
 
function ps_edit_columns($columns) {
  $columns = array(
    "cb" => "<input type='checkbox' />",
    "title" => __("Title"),
    "link" => __("Site"),
    "categories" => __("Categories"),
  );
 
  return $columns;
}

function ps_custom_columns($column, $post_id) {
    switch ($column) {
        case "link":
            $custom = get_post_custom($post_id);
            echo $custom["link"][0];
        break;
    }
}

// Registra o post type nas categorias.
register_taxonomy_for_object_type('category', 'ps');
function add_custom_types_to_tax( $query ) {
    if( is_category() || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {

    // Get all your post types
    //$post_types = get_post_types();

    // Pega somente os itens de menu e o post type "ps"
    $post_types = array('nav_menu_item', 'ps');

    $query->set( 'post_type', $post_types );
        return $query;
    }
}
add_filter( 'pre_get_posts', 'add_custom_types_to_tax' );
